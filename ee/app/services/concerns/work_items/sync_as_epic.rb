# frozen_string_literal: true

module WorkItems
  module SyncAsEpic
    SyncAsEpicError = Class.new(StandardError)
  end
end
